import { createApp } from 'vue';
import VueAxios from 'vue-axios';
import App from './App.vue';
import router from "./router";
import axios from "./api";
import store from "./vuex/store";

const app = createApp(App)
app.use(router)
app.use(VueAxios, axios)
app.use(store)
app.mount('#app')
