import { createStore } from 'vuex';
import axios from 'axios';

export default createStore({
    state () {
        return {
            tokens: null,
        }
    },
    mutations: {
        async REFTESH_TOKENS_FROM_API(state) {
            await axios.post(
                "http://0.0.0.0:8000/api/v1/auth/refresh/", {"refresh": state.tokens.refresh}
            ).then(response => {
                let refresh = state.tokens.refresh
                state.tokens = {
                    access: response.data.access,
                    refresh: refresh
                }
            });
        }
    },
    getters: {
        TOKENS(state) {
            return state.tokens;
        }
    }
})
