import { createRouter, createWebHashHistory } from "vue-router";

import CreateMessage from "./components/CreateMessage";
import InboxMsg from "./components/InboxMsg";
import LoginForm from "./components/LoginForm";
import SendedMsg from "./components/SendedMsg";
import RecipientMsg from "./components/RecipientMsg";

const routes = [
    { path: '/', component: InboxMsg },
    { path: '/create-msg', component: CreateMessage },
    { path: '/login', component: LoginForm },
    { path: '/sended', component: SendedMsg },
    { path: '/recipient', component: RecipientMsg },
  ]


export default createRouter({
    history: createWebHashHistory(),
    routes: routes
})
