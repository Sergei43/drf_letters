Test project for sending and receive messages

Tecnologies:
- Back-end (Django,Django Rest Framework);
- Front-end (Vue 3)
- Database (PostgreSQL)

Setup instraction:
1. In backend folder create .env file with config variables for example you cat use default.env file
2. Make Docker compose build project. Use command:
```shell
docker-compose build
```
3. Run Docker containers. Use comand:
```shell
docker-compose up -d
```